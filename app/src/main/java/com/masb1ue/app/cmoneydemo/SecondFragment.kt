package com.masb1ue.app.cmoneydemo

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.masb1ue.app.cmoneydemo.databinding.FragmentFirstBinding
import com.masb1ue.app.cmoneydemo.databinding.FragmentSecondBinding



class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    //滾動狀態
    private val _status = MutableLiveData<Int>()

    private val sharedViewModel: SharedViewModel by activityViewModels()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = sharedViewModel
        binding.dataGrid.adapter = DataAdapter()
//        binding.dataGrid.addOnScrollListener(object : RecyclerView.OnScrollListener(){
//            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                (binding.dataGrid.adapter as DataAdapter).scrollStatus =  newState
//            }
//
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                (binding.dataGrid.adapter as DataAdapter).scrollDistance =  dy
//                super.onScrolled(recyclerView, dx, dy)
//            }
//        })
        return view
    }
}