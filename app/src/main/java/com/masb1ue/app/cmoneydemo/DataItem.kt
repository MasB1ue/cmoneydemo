package com.masb1ue.app.cmoneydemo

class DataItem {
    var description: String = ""
    var copyright: String = ""
    var title: String = ""
    var url: String = ""
    var apod_site: String = ""
    var date: String = ""
    var media_type: String = ""
    var hdurl: String = ""

    constructor(
        description: String,
        copyright: String,
        title: String,
        url: String,
        apod_site: String,
        date: String,
        media_type: String,
        hdurl: String
    ) {
        this.description = description
        this.copyright = copyright
        this.title = title
        this.url = url
        this.apod_site = apod_site
        this.date = date
        this.media_type = media_type
        this.hdurl = hdurl
    }
}