package com.masb1ue.app.cmoneydemo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject


private const val DATA_URL = "https://raw.githubusercontent.com/cmmobile/NasaDataSet/main/apod.json"

enum class HttpStatus { LOADING, ERROR, DONE }

class SharedViewModel: ViewModel() {

    //儲存取得狀態
    private val _status = MutableLiveData<HttpStatus>()
    val status: LiveData<HttpStatus> = _status
    //儲存得到資料
    private val _dataList = MutableLiveData<List<DataItem>>()
    val dataList: LiveData<List<DataItem>> = _dataList

    init {
        getData()
    }

    private fun getData() {
        viewModelScope.launch(Dispatchers.IO) {
            _status.postValue(HttpStatus.LOADING)
            try {
                val service = HttpService()
                val data = service.sendGetRequest(DATA_URL)
                var jsonarray = JSONArray(data)
                var datalist: MutableList<DataItem> = mutableListOf()
                for (json: JSONObject in jsonarray) {
                    datalist.add(parserData(json))
                }
                _dataList.postValue(datalist)
                _status.postValue(HttpStatus.DONE)
            } catch (e: Exception) {
                print(e.message)
                _status.postValue(HttpStatus.ERROR)
                _dataList.postValue(listOf())
            }
        }
    }

    private fun parserData(json: JSONObject) : DataItem {
        var description = json.getString("description")
        var copyright = json.getString("copyright")
        var title = json.getString("title")
        var url = json.getString("url")
        var apod_site = json.getString("apod_site")
        var date = json.getString("date")
        var media_type = json.getString("media_type")
        var hdurl = json.getString("hdurl")
        return DataItem(description, copyright, title, url, apod_site, date, media_type, hdurl)
    }

    private operator fun JSONArray.iterator(): Iterator<JSONObject>  =
        (0 until length()).asSequence().map{ get(it) as JSONObject }.iterator()

}