package com.masb1ue.app.cmoneydemo

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class HttpService {

    @Throws(Exception::class)
    fun sendGetRequest(url: String, charset: String = "utf-8"): String {
        print("get url: ${url}")
        var resultBuffer = StringBuffer()
        var httpURLConnection = URL(url).openConnection() as HttpURLConnection
        httpURLConnection.setRequestProperty("Accept-Charset", charset)
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        if (httpURLConnection.responseCode >= 300) throw Exception("Http Request is not success, Response code is ${httpURLConnection.responseCode}")
        val inputStream = httpURLConnection.inputStream
        val inputStreamReader = InputStreamReader(inputStream, charset)
        val reader = BufferedReader(inputStreamReader)
        reader.forEachLine {
            if(it != "") resultBuffer.append(it)
        }
        reader.close()
        inputStreamReader.close()
        inputStream.close()
        return resultBuffer.toString()
    }

    @Throws(Exception::class)
    fun sendBitmapGetRequest(url: String, charset: String = "utf-8"): Bitmap {
        print("get url: ${url}")
        var httpURLConnection = URL(url).openConnection() as HttpURLConnection
        httpURLConnection.doInput = true
        httpURLConnection.connect()
        val inputStream = httpURLConnection.inputStream
        val bitmap = BitmapFactory.decodeStream(inputStream)
        inputStream.close()
        return bitmap
    }

    @Throws(Exception::class)
    fun sendBitmapGetRequest10(url: String, charset: String = "utf-8"): Bitmap {
        print("get url: ${url}")
        var httpURLConnection = URL(url).openConnection() as HttpURLConnection
        httpURLConnection.doInput = true
        httpURLConnection.connect()
        val inputStream = httpURLConnection.inputStream
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        options.inSampleSize = 10 //width，hight設為原來的十分一
        val bitmap = BitmapFactory.decodeStream(inputStream, null, options)
        inputStream.close()
        return bitmap!!
    }


}