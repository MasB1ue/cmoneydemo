package com.masb1ue.app.cmoneydemo

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.masb1ue.app.cmoneydemo.databinding.GridViewItemBinding

class DataAdapter : ListAdapter<DataItem, DataAdapter.DataViewHolder>(DiffCallback) {
//    var scrollStatus: Int = 0
//    var scrollDistance: Int = 0

    class DataViewHolder(private var binding: GridViewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(dataItem: DataItem) {
            binding.dataItem = dataItem
            binding.dataImage.setOnClickListener {
                val bundle = bundleOf(
                    "date" to dataItem.date,
                    "url" to dataItem.url,
                    "title" to dataItem.title,
                    "copyright" to dataItem.copyright,
                    "descript" to dataItem.description
                )
                //直接將文字資料傳給ThirdFragment
                it.findNavController().navigate(R.id.action_secondFragment_to_thirdFragment, bundle)
                }
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
            return oldItem.url == newItem.url
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        return DataViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
//        Log.d("onBindViewHolder", position.toString() +" " + scrollStatus + " " + scrollDistance)
            val dataItem = getItem(position)
            holder.bind(dataItem)
    }

//    override fun onViewRecycled(holder: DataViewHolder) {
//        super.onViewRecycled(holder)
//        Log.d("onViewRecycled", holder.itemView.toString())
//    }



}
