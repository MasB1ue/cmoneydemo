package com.masb1ue.app.cmoneydemo

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<DataItem>?) {
    val adapter = recyclerView.adapter as DataAdapter
    adapter.submitList(data)
}


@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val service = HttpService()
                val data = async { service.sendBitmapGetRequest10(imgUrl) }
                launch(Dispatchers.Main) { imgView.setImageBitmap(data.await()) }
            }catch(e: Exception) {
                print(e.message)
            }
        }

    }

}



@BindingAdapter("httpStatus")
fun bindStatus(statusImageView: ImageView, status: HttpStatus?) {
    when (status) {
        HttpStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        HttpStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        HttpStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}
