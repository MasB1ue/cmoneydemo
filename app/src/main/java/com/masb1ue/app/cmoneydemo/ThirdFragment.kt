package com.masb1ue.app.cmoneydemo

import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.masb1ue.app.cmoneydemo.databinding.FragmentFirstBinding
import com.masb1ue.app.cmoneydemo.databinding.FragmentThirdBinding
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*


class ThirdFragment : Fragment() {
    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    private val sharedViewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        val view = binding.root
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val formatter2 = SimpleDateFormat("yyyy MMM dd", Locale.ENGLISH)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = sharedViewModel
        binding.date.text = formatter2.format(formatter.parse(arguments?.getString("date")))
        binding.title.text = arguments?.getString("title")
        binding.copyrigh.text = getString(R.string.copyrightTitle) + arguments?.getString("copyright")
        binding.content.text = arguments?.getString("descript")
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val service = HttpService()
                val bitmap = async { service.sendBitmapGetRequest(arguments?.getString("url").toString()) }
                GlobalScope.launch(Dispatchers.Main) {
                    binding.image.setImageBitmap(bitmap.await()) }
            }catch(e: Exception){
                print(e.message)
            }

        }
        return view
    }
}